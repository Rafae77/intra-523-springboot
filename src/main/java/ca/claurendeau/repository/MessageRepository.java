package ca.claurendeau.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Message;


public interface MessageRepository extends JpaRepository<Message, Integer> {
	Message findByName(String name);
}

