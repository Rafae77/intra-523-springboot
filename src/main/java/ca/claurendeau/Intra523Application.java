package ca.claurendeau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import ca.claurendeau.service.Populator;

@SpringBootApplication
public class Intra523Application implements CommandLineRunner {
    
	@Autowired
	Populator populator;
	
	public static void main(String[] args) {
		SpringApplication.run(Intra523Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		populator.activatePopulator();
	}
	
	
}
