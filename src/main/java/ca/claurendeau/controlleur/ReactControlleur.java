package ca.claurendeau.controlleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.service.MessageService;

@CrossOrigin
@RestController
public class ReactControlleur {
    
	@Autowired
	private MessageService messageService;
	
    @GetMapping("/getUserByName/{name}")
	public Message findUserByName(@PathVariable String name) {
		return messageService.findByName(name);
	}
    
}
