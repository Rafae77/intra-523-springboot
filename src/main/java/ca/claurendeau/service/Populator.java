package ca.claurendeau.service;

import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Message;


@Service
public class Populator {
	@Autowired
	private MessageService messageService;
	
	public void activatePopulator(){
		Message mess = new Message();
		mess.setEmail("Rafa@");
		mess.setName("rafa");
		
		messageService.saveMessage(mess);
		
		Message mess1 = new Message();
		mess1.setEmail("Rafa1@");
		mess1.setName("rafa1");
		
		messageService.saveMessage(mess1);
		
		Message mess2 = new Message();
		mess2.setEmail("Rafa2@");
		mess2.setName("rafa2");
		
		messageService.saveMessage(mess2);
	}
}
