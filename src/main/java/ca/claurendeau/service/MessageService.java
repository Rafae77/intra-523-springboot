package ca.claurendeau.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.domaine.Message;
import ca.claurendeau.repository.MessageRepository;



@Service
public class MessageService {
	
	@Autowired
	private MessageRepository messageRepositry;
	
	public Message findByName(String name){
		return messageRepositry.findByName(name);
	}

	public Message saveMessage(Message message) {
		return messageRepositry.save(message);
	}
	
}
